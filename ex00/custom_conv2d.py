from turtle import forward
import numpy as np
from typing import Tuple
from numba import njit


class Param:
    def __init__(self, shape):
        self.data = np.random.rand(*shape)
        self.grad = np.zeros(shape)

    def zero_grad(self):
        self.grad = np.zeros_like(self.grad)


class Conv2D:
    def __init__(
        self,
        in_shape: Tuple[int, int, int],
        nb_kernels: int,
        kernel_size: int,
        stride: int = 1
    ) -> None:
        self.in_chan, h, w = in_shape
        self.out_chan = nb_kernels
        self.stride = stride
        
        self.in_dim = h, w
        self.out_dim = (h - kernel_size) // stride + 1, (w - kernel_size) // stride + 1

        # Weight shape: nb_kernels, nb_channels, kernel_size, kernel_size
        self.w = Param(shape=(self.out_chan, self.in_chan, kernel_size, kernel_size))

        # (PyTorch bias init) Bias shape: nb_kernels,
        self.b = Param(shape=(self.out_chan,))
        
        # (YouTube bias init) Bias shape: nb_kernels, input_width, input_height
        # self.b = Param(shape=(self.out_chan, *self.out_dim))

    def forward(self, inputs: np.ndarray) -> np.ndarray:
        # self.__assert_inputs(inputs)    # uncomment in production
        batch_size = inputs.shape[0]
        output = np.zeros((batch_size, self.out_chan, *self.out_dim))
        
        for b in range(batch_size):
            for k in range(self.out_chan):
                self.__sum_cross_correlation_valid(inputs[b], self.w.data[k], output[b, k], self.stride)
                output[b, k] += self.b.data[k]
        return output
        
    def __assert_inputs(self, inputs: np.ndarray) -> None:
        assert len(inputs.shape) == 4
        batch_size, in_chan, h, w = inputs.shape
        assert (in_chan, h, w) == (self.in_chan, *self.in_dim)

    @staticmethod
    @njit
    def __sum_cross_correlation_valid(
        inp: np.ndarray,    # single input
        ker: np.ndarray,    # single kernel
        out: np.ndarray,    # single output array
        stride: int         # stride on all dimensions
    ) -> None:
        out_h, out_w = out.shape
        for c in range(inp.shape[0]):
            mat = inp[c]
            ih = 0
            for y in range(out_h):
                iw = 0
                for x in range(out_w):
                    out[y, x] += np.sum(ker[c] * mat[ih:ih+ker.shape[1], iw:iw+ker.shape[1]])
                    iw += stride
                ih += stride

if __name__ == '__main__':
    # Doing example of the subject
    np.random.seed(42)
    in_shape = (3, 8, 10)
    x = np.random.rand(2, *in_shape)

    print(Conv2D(in_shape=in_shape, nb_kernels=1, kernel_size=3, stride=2).forward(x))
    print("\n" + ("." * 50) + "\n")
    print(Conv2D(in_shape=in_shape, nb_kernels=2, kernel_size=4).forward(x))